﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LendFoundry.TemplateManager.Persistence
{
    public class MongoTemplateManagerRepository : MongoRepository<ITemplate, Template>, ITemplateManagerRepository
    {
        static MongoTemplateManagerRepository()
        {
            BsonClassMap.RegisterClassMap<Template>(map =>
            {
                map.AutoMap();
                map.MapProperty(t => t.Properties).SetSerializer(new JsonSerializer<Dictionary<string, object>>());
                map.MapProperty(t => t.Format).SetSerializer(new EnumSerializer<Format>(BsonType.String));
                var type = typeof(Template);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public MongoTemplateManagerRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "templates")
        {
            CreateIndexIfNotExists("template-manager", Builders<ITemplate>.IndexKeys.Ascending(i => i.TenantId).Ascending(template => template.Name).Ascending(template => template.Version).Ascending(template => template.Format));
            CreateIndexIfNotExists("template-manager-no-version", Builders<ITemplate>.IndexKeys.Ascending(i => i.TenantId).Ascending(template => template.Name).Ascending(template => template.Format));
        }

        public Task<List<ITemplate>> GetAll()
        {
            return Query.ToListAsync();
        }

        public async Task<ITemplate> Get(string templateName, string version, Format format)
        {
            return await Query.FirstOrDefaultAsync(t => t.Name.ToLower() == templateName.ToLower() && t.Version.ToLower() == version.ToLower() && t.Format == format);
        }

        public async Task<Dictionary<string, object>> GetProperties(string templateName, string version, Format format)
        {
            return await Task.FromResult(Query.Where(t => t.Name.ToLower() == templateName.ToLower() && t.Version.ToLower() == version.ToLower() && t.Format == format).Select(t => t.Properties).FirstOrDefault());
        }

        public async Task<ITemplate> GetActiveTemplate(string templateName, Format format)
        {
            return await Query.FirstOrDefaultAsync(t => t.Name.ToLower() == templateName.ToLower() && t.IsActive == true && t.Format == format);
        }

        public async Task<List<ITemplate>> GetAllTemplateByName(string templateName, Format format)
        {
            return await Query.Where(t => t.Name.ToLower() == templateName.ToLower() && t.Format == format).ToListAsync();
        }
    }
}
