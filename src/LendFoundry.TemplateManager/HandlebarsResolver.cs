﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.TemplateManager
{
    public class HandlebarsResolver : IHandlebarsResolver
    {
        public void Start()
        {
            Register();
        }

        private static void Register()
        {
            HandlebarsDotNet.Handlebars.RegisterHelper("if-equal", (writer, options, context, arguments) =>
            {
                var arg1 =Convert.ToString( arguments[0]);
                var arg2 =Convert.ToString( arguments[1]);

                if (string.Equals(arg1,arg2))
                {
                    options.Template(writer, (object)context);
                }
                else
                {
                    options.Inverse(writer, (object)context);
                }
            });

            HandlebarsDotNet.Handlebars.RegisterHelper("if-is-null-or-empty", (writer, options, context, arguments) =>
            {
                var arg1 =Convert.ToString( arguments[0]);

                if (string.IsNullOrEmpty(arg1))
                {
                    options.Template(writer, (object)context);
                }
                else
                {
                    options.Inverse(writer, (object)context);
                }
            });

            HandlebarsDotNet.Handlebars.RegisterHelper("if-is-not-null-or-non-empty", (writer, options, context, arguments) =>
            {
                var arg1 =Convert.ToString( arguments[0]);

                if (!string.IsNullOrEmpty(arg1))
                {
                    options.Template(writer, (object)context);
                }
                else
                {
                    options.Inverse(writer, (object)context);
                }
            });

        }
    }
    
}
