using HandlebarsDotNet;

namespace LendFoundry.TemplateManager
{
    public class HandlebarsTemplateProcessor : ITemplateProcessor
    {
        public string Process(string body, object data)
        {
            var template = Handlebars.Compile(body);
            return template(data ?? new object());
        }
    }
}