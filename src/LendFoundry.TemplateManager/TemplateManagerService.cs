﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;

namespace LendFoundry.TemplateManager
{
    public class TemplateManagerService : ITemplateManagerService
    {
        public TemplateManagerService(ITemplateManagerRepository templateRepository, ITemplateProcessor templateProcessor, IEventHubClient eventHub, IConfigurationServiceFactory configurationServiceFactory, ITokenReader tokenReader, ITokenHandler tokenHandler, ITenantTime tenantTime)
        {
            if (templateRepository == null)
                throw new ArgumentNullException(nameof(templateRepository));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));

            TemplateRepository = templateRepository;
            TemplateProcessor = templateProcessor;
            EventHub = eventHub;
            ConfigurationServiceFactory = configurationServiceFactory;
            TokenReader = tokenReader;
            TokenHandler = tokenHandler;
            TenantTime = tenantTime;
        }

        private ITemplateManagerRepository TemplateRepository { get; }
        private ITemplateProcessor TemplateProcessor { get;}
        private IEventHubClient EventHub { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }
        private ITenantTime TenantTime { get; }
        // Sample value for place holder will be @@##LoanAgreement##v1##html##@@
        private const string TEMPLATE_PLACEHOLDER_REGEX = "@@##[^\\s]+?##@@";
       
        public async Task<ITemplate> Get(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof (Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var template=  await TemplateRepository.Get(templateName,version,format);
            if(template==null)
                throw new NotFoundException("Template not found");
            return template;
        }

        public async Task<Dictionary<string, object>> GetProperties(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var properties = await TemplateRepository.GetProperties(templateName, version, format);
            if (properties == null)
                throw new NotFoundException("Template not found");
            return properties;
        }

        public Task<List<ITemplate>> GetAll()
        {
            return TemplateRepository.GetAll();
        }

        public async Task<ITemplate> Add(ITemplate template)
        {
            EnsureTemplateIsValid(template);

            if (await TemplateRepository.Get(template.Name, template.Version, template.Format) != null)
                throw new TemplateAlreadyExists("Template with same name is already exist");
                        
            if (template.IsActive == false)
            {
                //User is creating new version and passed IsActive as false
                //We will keep it as active false
                var existingTemplates = await TemplateRepository.GetAllTemplateByName(template.Name, template.Format);
                if (!existingTemplates.Any())                
                {
                    //if this is new template and no existing version exist it will become active. It's just for backward compatibility
                    template.IsActive = true;
                }
            }
            
            if (template.IsActive)
            {
                //Only deactivate other template if active passed as true
                await DeactivateAllTemplates(template.Name, template.Format);   
            }
            
            template.CreatedDate = new TimeBucket(TenantTime.Now);
            template.CreatedBy = ExtractCurrentUser();
            TemplateRepository.Add(template);

            await EventHub.Publish(new Events.TemplateAdded
            {
                Template = template
            });

            return template;
        }

        public async Task Update(string templateName, string version, Format format, ITemplate template)
        {
            EnsureTemplateIsValid(template);

            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            var existingTemplate = await Get(templateName,version,format);
            existingTemplate.Title = template.Title;
            existingTemplate.Body = template.Body;
            existingTemplate.Format = template.Format;
            existingTemplate.Properties = template.Properties;
            existingTemplate.IsSystem = template.IsSystem;
            existingTemplate.TemplateType = template.TemplateType;
            existingTemplate.ModifiedBy = ExtractCurrentUser();
            existingTemplate.ModifiedDate = new TimeBucket(TenantTime.Now);
            
            if (template.IsActive && existingTemplate.IsActive==false)
            {
                existingTemplate.IsActive = true;
                await DeactivateAllTemplates(template.Name, template.Format);   
            }
            
            TemplateRepository.Update(existingTemplate);

            await EventHub.Publish(new Events.TemplateUpdated
            {
                Template = template
            });
        }

        public async Task Delete(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var existingTemplate =  await Get(templateName,version,format);
            
            if(existingTemplate.IsActive)
            {
                throw new ArgumentException($"Active template cannot be deleted");
            }
            TemplateRepository.Remove(existingTemplate);

            await  EventHub.Publish(new Events.TemplateRemoved
            {
                Template = existingTemplate
            });
        }

        public async Task<ITemplateResult> Process(string templateName, string version, Format format, object data)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var template = await TemplateRepository.Get(templateName, version, format);

            if(template == null)
                throw new NotFoundException("Template not found");

            return await Process(template, data);
        }

        public async Task<ITemplateResult> ProcessActiveTemplate(string templateName, Format format, object data)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var template = await TemplateRepository.GetActiveTemplate(templateName, format);

            if(template == null)
                throw new NotFoundException("Template not found");
            
            return await Process(template, data);
        }

        public async Task ActivateTemplate(string templateName, string version, Format format)
        {
            var template = await Get(templateName, version, format);
            if(template.IsActive)
            {
                throw new ArgumentException($"Template is already active");
            }
            template.IsActive = true;
            await DeactivateAllTemplates(templateName, format);
            TemplateRepository.Update(template);
        }

        public async Task DeactivateTemplate(string templateName, string version, Format format)
        {
            var template = await Get(templateName, version, format);
            if(!template.IsActive)
            {
                throw new ArgumentException($"Template is already de-active");
            }            
            template.IsActive = false;
            TemplateRepository.Update(template);
        }

        public async Task<ITemplate> GetActiveTemplate(string templateName, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (!Enum.IsDefined(typeof (Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var template = await TemplateRepository.GetActiveTemplate(templateName,format);
            
            if(template == null)
                throw new NotFoundException("Template not found");

            return template;
        }

        private static void EnsureTemplateIsValid(ITemplate template)
        {
            if (template == null)
                throw new ArgumentNullException(nameof(template));

            if (string.IsNullOrWhiteSpace(template.Name))
                throw new ArgumentException("Template name is required", nameof(template.Name));

            if (string.IsNullOrWhiteSpace(template.Body))
                throw new ArgumentException("Template body is required", nameof(template.Body));

            if (string.IsNullOrWhiteSpace(template.Version))
                throw new ArgumentException("Argument is null or whitespace", nameof(template.Version));

            if (!Enum.IsDefined(typeof(Format), template.Format))
                throw new ArgumentOutOfRangeException(nameof(template.Format));
        }

        private async Task<ITemplateResult> Process (ITemplate template, object data)
        {
            var titleResult = string.Empty;

            var tenantConfiguration = ConfigurationServiceFactory.Create<object>("tenant", TokenReader).Get();

            if (data != null)
            {
                dynamic dynamic = data;
                dynamic.tenantConfiguration = tenantConfiguration;
                data = dynamic;
            }
            else
            {
                dynamic dynamic = new ExpandoObject();
                dynamic.tenantConfiguration = tenantConfiguration;
                data = dynamic;
            }
            var body = await ProcessNestedtemplate(template.Body, new List<ITemplate>{template});
            if (!string.IsNullOrWhiteSpace(template.Title))
            {
                titleResult = TemplateProcessor.Process(template.Title, data);
            }

            var bodyResult = TemplateProcessor.Process(body, data);

            return new TemplateResult { Title = titleResult, Data = bodyResult, Properties = template.Properties };
        }

        private async Task DeactivateAllTemplates(string templateName, Format format)
        {
            var templates = await TemplateRepository.GetAllTemplateByName(templateName, format);
            if(templates == null || !templates.Any())
            {
                return;
            }
            foreach (var template in templates)
            {
                template.IsActive = false;
                TemplateRepository.Update(template);
            }
        }

        private async Task<string> ProcessNestedtemplate(string body, List<ITemplate> parentTemplates)
        {
            var matches = Regex.Matches(body, TEMPLATE_PLACEHOLDER_REGEX, RegexOptions.IgnoreCase);
            if(matches.Count > 0)
            {
                foreach (var match in matches)
                {
                    var data = Convert.ToString(match).Replace("@@##","").Replace("##@@","");
                    var templateDetails = Regex.Split(data,"##");
                    if(templateDetails.Length < 2)
                    {
                        throw new ArgumentException($"{Convert.ToString(match)} invalid format for template token");
                    }
                    var templateName = templateDetails[0];
                    var version = templateDetails.Length > 2 ? templateDetails[1] : string.Empty;
                    var format = templateDetails.Length > 2 ? templateDetails[2] : templateDetails[1];

                    Format templateFormat;
                    var formatCheck = Enum.TryParse(format, true, out templateFormat);
                    if(!formatCheck)
                    {
                        throw new ArgumentException($"Invalid template format value {format}");
                    }
                    ITemplate template = null;
                    if(!string.IsNullOrWhiteSpace(version))
                    {
                        template = await TemplateRepository.Get(templateName, version, templateFormat);
                    }
                    else
                    {
                        template = await TemplateRepository.GetActiveTemplate(templateName, templateFormat);
                    }
                    if(template == null)
                    {
                        if(string.IsNullOrWhiteSpace(version))
                        {
                            throw new ArgumentException($"Template not found with name: {templateName} and format: {format} which is specified in body");    
                        }
                        throw new ArgumentException($"Template not found with name: {templateName}, version: {version} and format: {format} which is specified in body");
                    }
                    if(parentTemplates.Any(p=>p.Name == template.Name && p.Version == template.Version && p.Format == template.Format))
                    {
                        throw new ArgumentException($"Invalid nesting of templates");
                    }

                    body = body.Replace(Convert.ToString(match), await ProcessNestedtemplate(template.Body, new List<ITemplate>(parentTemplates) {template}));
                }                
            }
            return body;
        }

        private string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                username = string.Empty;            // In case of master token, subject would be null, hence keeping username as blank.
            return username;
        }
    }
}
