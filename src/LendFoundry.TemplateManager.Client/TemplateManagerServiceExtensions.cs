using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.TemplateManager.Client
{
    public static class TemplateManagerServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddTemplateManagerService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ITemplateManagerServiceFactory>(p => new TemplateManagerServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITemplateManagerServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddTemplateManagerService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<ITemplateManagerServiceFactory>(p => new TemplateManagerServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<ITemplateManagerServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddTemplateManagerService(this IServiceCollection services)
        {
            services.AddTransient<ITemplateManagerServiceFactory>(p => new TemplateManagerServiceFactory(p));
            services.AddTransient(p => p.GetService<ITemplateManagerServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}