﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;

namespace LendFoundry.TemplateManager.Client
{
    public class TemplateManagerService : ITemplateManagerService
    {
        public TemplateManagerService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<ITemplate> Get(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            return await Client.GetAsync<Template>($"/{templateName}/{version}/{format}");
        }

        public async Task<Dictionary<string, object>> GetProperties(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            return await Client.GetAsync<Dictionary<string, object>>($"/{templateName}/{version}/{format.ToString()}/properties");
        }

        public async Task<List<ITemplate>> GetAll()
        {
            var result = await Client.GetAsync<List<Template>>("/");
            return new List<ITemplate>(result);
        }

        public async Task<ITemplate> Add(ITemplate template)
        {
            return await Client.PostAsync<ITemplate, Template>("/", template, true);
        }

        public async Task Update(string templateName, string version, Format format, ITemplate template)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            await Client.PutAsync<ITemplate, Template>($"/{templateName}/{version}/{format.ToString()}", template, true);
        }

        public async Task Delete(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            await Client.DeleteAsync($"/{templateName}/{version}/{format.ToString()}");
        }

        public async Task<ITemplateResult> Process(string templateName, string version, Format format, object data)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            return await Client.PostAsync<object, TemplateResult>($"{templateName}/{version}/{format.ToString()}/process", data, true);
        }

        public async Task<ITemplateResult> ProcessActiveTemplate(string templateName, Format format, object data)
        {
            return await Client.PostAsync<object, TemplateResult>($"{templateName}/{format.ToString()}/process", data, true);
        }

        public async Task ActivateTemplate(string templateName, string version, Format format)
        {
            await Client.PostAsync<object, object>($"{templateName}/{version}/{format.ToString()}/activate", null);
        }

        public async Task DeactivateTemplate(string templateName, string version, Format format)
        {
            await Client.PostAsync<object, object>($"{templateName}/{version}/{format.ToString()}/deactivate", null);
        }
    }

}
