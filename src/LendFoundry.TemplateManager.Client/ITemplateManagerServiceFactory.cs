using LendFoundry.Security.Tokens;

namespace LendFoundry.TemplateManager.Client
{
    public interface ITemplateManagerServiceFactory
    {
        ITemplateManagerService Create(ITokenReader reader);
    }
}