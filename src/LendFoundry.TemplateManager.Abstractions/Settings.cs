using System;

namespace LendFoundry.TemplateManager
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "template-manager";

    }
}