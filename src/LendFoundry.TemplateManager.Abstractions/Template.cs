﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.TemplateManager
{
    public class Template : Aggregate, ITemplate
    {
        public Template()
        {
        }

        public Template(TemplateRequest templateRequest)
        {
            Name = templateRequest.Name;
            Title = templateRequest.Title;
            Format = templateRequest.Format;
            Body = templateRequest.Body;
            Version = templateRequest.Version;
            Properties = templateRequest.Properties;
            IsActive = templateRequest.IsActive;
            IsSystem = templateRequest.IsSystem;
            TemplateType = templateRequest.TemplateType;
        }

        public string Name { get; set; }
        public string Title { get; set; }
        public Format Format { get; set; }
        public string Body { get; set; }
        public string Version { get; set; }
        public Dictionary<string, object> Properties { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public string TemplateType { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public TimeBucket CreatedDate { get; set; }
        public TimeBucket ModifiedDate { get; set; }
    }
}
