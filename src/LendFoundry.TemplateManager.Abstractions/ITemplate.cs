﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.TemplateManager
{        
    public interface ITemplate : IAggregate
    {
        string Name { get; set; }
        string Title { get; set; }
        Format Format { get; set; }
        string Body { get; set; }
        string Version { get; set; }
        Dictionary<string, object> Properties { get; set; }
        bool IsActive { get; set; }
        bool IsSystem { get; set; }
        string TemplateType { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
        TimeBucket CreatedDate { get; set; }
        TimeBucket ModifiedDate { get; set; }
    }
}
