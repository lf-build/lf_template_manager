using System.Collections.Generic;

namespace LendFoundry.TemplateManager
{
    public interface ITemplateResult
    {
        string Title { get; set; }
        string Data { get; set; }
        Dictionary<string, object> Properties { get; set; }
    }
}