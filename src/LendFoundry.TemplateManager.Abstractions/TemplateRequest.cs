﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.TemplateManager
{
    public class TemplateRequest
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public Format Format { get; set; }
        public string Body { get; set; }
        public string Version { get; set; }
        public Dictionary<string, object> Properties { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public string TemplateType { get; set; }
    }
}
