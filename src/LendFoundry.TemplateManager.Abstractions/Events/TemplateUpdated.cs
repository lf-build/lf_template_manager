﻿namespace LendFoundry.TemplateManager.Events
{
    public class TemplateUpdated 
    {
        public ITemplate Template { get; set; }
    }
}
