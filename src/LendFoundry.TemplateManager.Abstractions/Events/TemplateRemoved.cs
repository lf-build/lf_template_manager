﻿namespace LendFoundry.TemplateManager.Events
{
    public class TemplateRemoved 
    {
        public ITemplate Template { get; set; }
    }
}
