﻿namespace LendFoundry.TemplateManager.Events
{
    public class TemplateAdded 
    {
        public ITemplate Template { get; set; }
    }
}
