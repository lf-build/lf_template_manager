using System.Collections.Generic;

namespace LendFoundry.TemplateManager
{
    public class TemplateResult : ITemplateResult
    {
        public string Title { get; set; }
        public string Data { get; set; }
        public Dictionary<string, object> Properties { get; set; }
    }
}