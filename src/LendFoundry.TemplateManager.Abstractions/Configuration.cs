﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.TemplateManager
{
    public class Configuration : IDependencyConfiguration
    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
