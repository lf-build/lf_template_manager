﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;

namespace LendFoundry.TemplateManager.Api.Tests
{
    public class MockTemplateManagerService : ITemplateManagerService
    {
        private IDictionary<string, ITemplate> Templates { get; } = new Dictionary<string, ITemplate>
        {
            {
                "existing.template_1.0_Text",
                new Template
                {
                    Name = "existing.template",
                    Body = "test",
                    Format = Format.Text,
                    Version = "1.0",
                    Properties = new Dictionary<string, object>()
                     {
                         { "pdfSettings", new Dictionary<string,string>(){{ "pageSize", "A4"},{ "orientation", "portrait"}}}
                     }

                }
            },
            {
                "existing.template_Text",
                new Template
                {
                    Name = "existing.template",
                    Body = "test",
                    Format = Format.Text,
                    Version = "1.0",
                    Properties = new Dictionary<string, object>()
                     {
                         { "pdfSettings", new Dictionary<string,string>(){{ "pageSize", "A4"},{ "orientation", "portrait"}}}
                     }

                }
            },
            {
                "existing.template_1.0_Text_Active",
                new Template
                {
                    Name = "existing.template",
                    Body = "test",
                    Format = Format.Text,
                    Version = "1.0",
                    Properties = new Dictionary<string, object>()
                     {
                         { "pdfSettings", new Dictionary<string,string>(){{ "pageSize", "A4"},{ "orientation", "portrait"}}}
                     },
                     IsActive = true

                }
            },
            {
                "existing.template_1.0_Text_Deactive",
                new Template
                {
                    Name = "existing.template",
                    Body = "test",
                    Format = Format.Text,
                    Version = "1.0",
                    Properties = new Dictionary<string, object>()
                     {
                         { "pdfSettings", new Dictionary<string,string>(){{ "pageSize", "A4"},{ "orientation", "portrait"}}}
                     },
                     IsActive = false

                }
            }
        };

        public Task<ITemplate> Get(string templateName, string version, Format format)
        {
            if (Templates.ContainsKey($"{templateName}_{version}_{format}"))
                return Task.FromResult(Templates[$"{templateName}_{version}_{format}"]);

            throw new NotFoundException(string.Empty);
        }

        public Task<Dictionary<string, object>> GetProperties(string templateName, string version, Format format)
        {
            if (Templates.ContainsKey($"{templateName}_{version}_{format}"))
                return Task.FromResult(Templates[$"{templateName}_{version}_{format}"].Properties);

            throw new NotFoundException(string.Empty);
        }

        public Task<List<ITemplate>> GetAll()
        {
            return Task.FromResult(Templates.Values.ToList());
        }

        public Task<ITemplate> Add(ITemplate template)
        {
            if (Templates.ContainsKey($"{template.Name}_{template.Version}_{template.Format}"))
                throw new TemplateAlreadyExists();

            var templateId = template.Name;
            Templates.Add(templateId, template);
            return Task.FromResult(template);
        }

        public Task Update(string templateName, string version, Format format, ITemplate template)
        {
            if (Templates.ContainsKey($"{templateName}_{version}_{format}"))
            {
                Templates[$"{templateName}_{version}_{format}"] = template;
                return Task.FromResult(0);
            }
            throw new NotFoundException(string.Empty);
        }

        public Task Delete(string templateName, string version, Format format)
        {
            if (Templates.ContainsKey($"{templateName}_{version}_{format}"))
            {
                Templates.Remove($"{templateName}_{version}_{format}");
                return Task.FromResult(0);
            }
            throw new NotFoundException(string.Empty);
        }

        public Task<ITemplateResult> Process(string templateName, string version, Format format, object data)
        {
            if (Templates.ContainsKey($"{templateName}_{version}_{format}"))
            {
                return Task.FromResult<ITemplateResult>(new TemplateResult { Data = "success" });
            }

            throw new NotFoundException(string.Empty);
        }

        public Task<ITemplateResult> ProcessActiveTemplate(string templateName, Format format, object data)
        {
            if (Templates.ContainsKey($"{templateName}_{format}"))
            {
                return Task.FromResult<ITemplateResult>(new TemplateResult { Data = "success" });
            }

            throw new NotFoundException(string.Empty);
        }

        public Task ActivateTemplate(string templateName, string version, Format format)
        {
            if (Templates.ContainsKey($"{templateName}_{version}_{format}_Active"))
            {
                return Task.FromResult<ITemplateResult>(new TemplateResult { Data = "success" });
            }

            throw new NotFoundException(string.Empty);
        }

        public Task DeactivateTemplate(string templateName, string version, Format format)
        {
            if (Templates.ContainsKey($"{templateName}_{version}_{format}_Deactive"))
            {
                return Task.FromResult<ITemplateResult>(new TemplateResult { Data = "success" });
            }

            throw new NotFoundException(string.Empty);
        }
    }
}
