﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using Xunit;

namespace LendFoundry.TemplateManager.Client.Tests
{
    public class TemplateManagerClientTests
    {
        private ITemplateManagerService TemplateManagerClient { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> MockServiceClient { get; }

        public TemplateManagerClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            TemplateManagerClient = new TemplateManagerService(MockServiceClient.Object);
        }

        [Fact]
        public async void Client_Get_Template()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<Template>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        Task.FromResult(new Template { Name = "existing.template", Body = "test", Format = Format.Text }));

            var result = await TemplateManagerClient.Get("test.template", "v1.0", Format.Text);
            Assert.Equal("/{templateName}/{version}/{format}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_Get_Template_ThrowsException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.Get(string.Empty, null, Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.Get("test", null, Format.Text));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => TemplateManagerClient.Get("test", "1.0", (Format)100));
        }

        [Fact]
        public async void Client_GetProperties_Template()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<Dictionary<string, object>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        Task.FromResult(new Dictionary<string, object>()
                     {
                         { "pdfSettings", new Dictionary<string,string>(){{ "pageSize", "A4"},{ "orientation", "portrait"}}}
                     }));

            var result = await TemplateManagerClient.GetProperties("test.template", "v1.0", Format.Text);
            Assert.Equal("/{templateName}/{version}/{format}/properties", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_GetProperties_Template_ThrowsException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.GetProperties(string.Empty, null, Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.GetProperties("test", null, Format.Text));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => TemplateManagerClient.GetProperties("test", "1.0", (Format)100));
        }

        [Fact]
        public async void Client_GetAll_Templates()
        {
            var templates = new List<Template>
            {
                new Template { Name = "existing.template", Body = "test", Format = Format.Text },
                new Template { Name = "existing2.template", Body = "test", Format = Format.Text }
            };

            MockServiceClient.Setup(s => s.ExecuteAsync<List<Template>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        Task.FromResult(templates));

            var result = await TemplateManagerClient.GetAll();
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotEmpty(result);
        }

        [Fact]
        public async void Client_Add_Template()
        {
            var template = new Template { Name = "new.template", Body = "test", Format = Format.Text };

            MockServiceClient.Setup(s => s.ExecuteAsync<Template>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        Task.FromResult(template));

            var result = await TemplateManagerClient.Add(template);
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_Update_Template()
        {
            var templateName = "existing.template";

            var template = new Template { Name = "existing.template", Body = "test", Format = Format.Text };

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r);

            TemplateManagerClient.Update(templateName, "1.0", Format.Text, template);
            Assert.Equal("/{templateName}/{version}/{format}", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_Update_Template_ThrowsException()
        {
            var template = new Template { Name = "existing.template", Body = "test", Format = Format.Text };

            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.Update(string.Empty, null, Format.Text, template));
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.Update("test", null, Format.Text, template));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => TemplateManagerClient.Update("test", "1.0", (Format)100, template));
        }

        [Fact]
        public void Client_Delete_Template()
        {
            var templateName = "existing.template";

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r);

            TemplateManagerClient.Delete(templateName, "1.0", Format.Text);
            Assert.Equal("/{templateName}/{version}/{format}", Request.Resource);
            Assert.Equal(Method.DELETE, Request.Method);
        }

        [Fact]
        public async void Client_Delete_Template_ThrowsException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.Delete(string.Empty, null, Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.Delete("test", null, Format.Text));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => TemplateManagerClient.Delete("test", "1.0", (Format)100));
        }

        [Fact]
        public async void Client_Process_Template()
        {
            var templateName = "existing.template";

            MockServiceClient.Setup(s => s.ExecuteAsync<TemplateResult>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        Task.FromResult(new TemplateResult { Data = "test" }));

            var result = await TemplateManagerClient.Process(templateName, "v1.0", Format.Text, new { data = "test" });
            Assert.Equal("{templateName}/{version}/{format}/process", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public async void Client_Process_Template_ThrowsException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.Process(string.Empty, null, Format.Text, new { data = "test" }));
            await Assert.ThrowsAsync<ArgumentException>(() => TemplateManagerClient.Process("test", null, Format.Text, new { data = "test" }));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => TemplateManagerClient.Process("test", "1.0", (Format)100, new { data = "test" }));
        }
    }
}
